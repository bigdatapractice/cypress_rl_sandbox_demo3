  // ***********************************************
  // This example commands.js shows you how to
  // create various custom commands and overwrite
  // existing commands.
  //
  // For more comprehensive examples of custom
  // commands please read more here:
  // https://on.cypress.io/custom-commands
  // ***********************************************
  //
  //
  // -- This is a parent command --
  // Cypress.Commands.add("login", (email, password) => { ... })
  //
  //
  // -- This is a child command --
  // Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
  //
  //
  // -- This is a dual command --
  // Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
  //
  //
  // -- This will overwrite an existing command --
  // Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

  Cypress.Commands.add("testcharts",(chartId)=>{
      cy.get(chartId)
      .and(chart => {
        // we can assert anything about the chart really
        expect(chart.height()).to.be.greaterThan(30)
      })
  })  

  Cypress.Commands.add("tablecheck",(tableId)=>{
      cy.get(tableId).find('tr').its('length').should('be.gte', 0)

  })
  Cypress.Commands.add("login",(username,password,url)=>{
    cy.visit(url);
    cy.get('#username')
    .type(username)
    .should('have.value', username)
    cy.wait(3000)
    cy.get('#password')
    .type(password)
    .should('have.value', password)
    cy.wait(3000)
    cy.get('[type="submit"]').click() ;
    
  })

  Cypress.Commands.add("logout",()=>{
    cy.get('#mainNavigationMenu').trigger('mouseover')
    cy.get('#userLogout').click()
    cy.get('#bot2-Msg1').click()
    cy.wait(2000)
    cy.title().should('eq','Select Application to Proceed')
  })

  Cypress.Commands.add('pageLoadCheck',(url)=>{
    cy.visit(url);
    Cypress.on('uncaught:exception', (err, runnable) => {
    return false
  });
  Cypress.Cookies.defaults({
    whitelist: "JSESSIONID"
  })
  cy.wait(2000)
  })

Cypress.Commands.add('refineSearch',(typableText)=>{
    cy.window().scrollTo('top')
    cy.wait(5000)
    cy.get('.refineSearch').click()
    if(cy.get('#f1 > label > div > div > button').should('not.be.visible')){
        cy.get('.refineSearch').click()
    }
    cy.get('#f1 > label > div > div > button').click()
    cy.get('#f1 > label > div > div > ul > div > input').type(typableText)
    cy.get('#f1 > label > div > div > ul > li:nth-child(3) > a > label > input[type=checkbox]').check()
    cy.get('#applyFilter').click()
})