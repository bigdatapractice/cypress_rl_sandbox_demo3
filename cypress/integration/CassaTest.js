
describe('Should load page', function () {
    it('Page Loading', function () {
        cy.pageLoadCheck('login?app=ROLE_CELGENE')
    })
});

describe('Log-In Verification', function () {
    it('should add valid username and password', function () {
        cy.login('demo', 'demo', 'login?app=ROLE_CELGENE')
    })
})

describe('Summary dashboard', function () {
    it('Testing widgets', function () {
        //checking widget load usibg labels
        cy.visit('index#LandingPageProfile')
        cy.wait(20000)
        cy.get('#summary12876').contains('span', 'Compounds')
        cy.get('#summary22876').contains('span', 'Studies')
        cy.get('#summary32876').contains('span', 'Countries')
        cy.get('#summary12902').contains('span', 'Open Planned Orders')
        cy.get('#summary12903').contains('span', 'Open Firm Orders')

        //scrolable check and pie chart chart
        cy.window().scrollTo('bottom')
        cy.get('#datatable_12876').should('be.visible')
        cy.get('#c32899').find('.pie-slice-group').children().its('length').should('be.gte', 2)


        cy.get('#c32876container > svg > g > g.chart-body > g.stack._0').children().its('length').should('be.gte', 50)
        cy.get('#datatable_12876_info > span.text-primary').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.greaterThan(1000)

        })
    })
    it(' Dashboard data storage ', function () {
        let dashBoardData = {
            compound: "0",
            studies: "0",
            countries: "0",
            open_Planned_orders: "0",
            open_firm_orders: "0"


        }
        cy.get('#summary12876').find('h2').invoke('text').then(someText => {
            dashBoardData['compound'] = someText

        })

        cy.get('#summary22876').find('h2').invoke('text').then(someText => {
            dashBoardData['studies'] = someText

        })

        cy.get('#summary32876').find('h2').invoke('text').then(someText => {
            dashBoardData['countries'] = someText
        })

        cy.get('#summary12902').find('h2').invoke('text').then(someText => {
            dashBoardData['open_Planned_orders'] = someText
        })

        cy.get('#summary12903').find('h2').invoke('text').then(someText => {
            dashBoardData['open_firm_orders'] = someText
        })
        cy.wait(5000)

        cy.writeFile('example.json', dashBoardData)



    })

    it('Filter test case', function () {
        cy.window().scrollTo('top')
        cy.wait(5000)
        cy.get('.refineSearch').click()
        cy.get('#f1 > label > div > div > button').click()
        cy.get('#f1 > label > div > div > ul > div > input').type('BZ')
        cy.get('#f1 > label > div > div > ul > div > input').type('{enter}')
        cy.get('#downloadExcel').should('be.visible').click()


    })



    it('Testing IIT functionality', function () {

        cy.wait(5000)
        cy.get('.refineClear').click()
        cy.get('#summary22876 > div > div.row.no-margin > div.col-xs-9.font-bold > h2 > div > a:nth-child(6)').click()
        cy.wait(6000)
        cy.get('#datatable_12958 > tbody > tr:nth-child(1) > td.sorting_1 > a').invoke('text').then(sometext => {
            const selectedStudy = sometext
            cy.get('#datatable_12958 > tbody > tr:nth-child(1) > td.sorting_1 > a').click()
            cy.wait(10000)
            cy.url().should('include', 'StudyScreen_IIT')
            cy.get('#filterLastLabel').invoke('text').should('eq', selectedStudy)

        })


    })


})



describe('Compound Analysis', function () {
    it('Date functionality', function () {
        cy.visit('index#Compound_Info_Screen')
        cy.wait(20000)
        const todaysDate = Cypress.moment().format('MM/DD/YYYY')
        cy.get('#f2 > label > div > div:nth-child(1) > label > input').should('have.value', todaysDate)

        var futureDate = Cypress.moment().add(2, 'year').format('MM/DD/YYYY')
        cy.get('#f2 > label > div > div:nth-child(2) > label > input').should('have.value', futureDate)

    })


    it('Testing widgets', function () {
        cy.get('#c22885').scrollIntoView()
            .should('be.visible')



    })

    it('Planned and Firm Orders by material numbers', function () {
        cy.get('#c12887 > svg').should('be.visible')
            .and(chart => {
                // we can assert anything about the chart really
                expect(chart.height()).to.be.greaterThan(100)
            })

    })

    it('Planned and Firm Orders by material numbers', function () {
        cy.get('#c12887 > svg').should('be.visible')
            .and(chart => {
                // we can assert anything about the chart really
                expect(chart.height()).to.be.greaterThan(100)
            })

    })

    it('Production Plan', function () {
        cy.get('#c12885 > svg').should('be.visible')
            .and(chart => {
                // we can assert anything about the chart really
                expect(chart.height()).to.be.greaterThan(100)

            })

    })

    it('Monthly Production Plan', function () {
        cy.get('#datatable_12885_info > span.text-primary').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.greaterThan(4000)

        })
    })

    it('Quaterly Production Plan', function () {
        cy.get('#datatable_12872_info > span.text-primary').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.greaterThan(1800)

        })
    })

    it('Summary studies by compound', function () {
        var studiesRawString, splittedIndex, studiesValue
        cy.readFile('example.json').then((data) => {
            studiesRawString = data.studies
            splittedIndex = studiesRawString.lastIndexOf(')')
            studiesValue = studiesRawString.substr(splittedIndex + 1)

            cy.get('#datatable_12858_info > span.text-primary').invoke('text').then(sometext => {
                let CompoundStudiesValue = sometext
                expect(CompoundStudiesValue).to.be.equal(studiesValue)
            })

        })

    })


})

describe('CR and D study analysis', function () {

    it('summary check', function () {
        cy.visit('index#StudyScreen')
        cy.wait(20000)

        cy.get('#summary12847 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(25000, 35000)
        })

        cy.get('#summary22847 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(13000, 20000)
        })

        cy.get('#summary32847 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(0.15, 0.21)
        })

        cy.get('#summary42847 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(0.40, 0.55)

        })
        cy.get('#summary52847 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(7000, 8000)

        })

        cy.get('#summary62847 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(6000, 7000)

        })

    })


    it('filter check', function () {
        cy.get('#f1 > label > div > div > button').click({ force: true })
        cy.get('#f1 > label > div > div > ul > div > input').type('BZ', { force: true })
        cy.get('#f1 > label > div > div > ul > li:nth-child(3) > a > label > input[type=checkbox]').check({ force: true })
        cy.get('#applyFilter').click()
        cy.wait(10000)
        
        //testing widgets
        cy.tablecheck('#datatable_12847')
        cy.tablecheck('#datatable_12846')
        cy.testcharts('#c12848 > svg')
        cy.tablecheck('#datatable_12848')
        cy.tablecheck('#datatable_12849')
        cy.testcharts('#c12855container > svg')
        cy.testcharts('#c22855container > svg')
        cy.testcharts('#c32855container > svg')
        cy.tablecheck('#datatable_12855')
        cy.testcharts('#c12888 > svg')
        cy.tablecheck('#datatable_12888')


    })


})
describe('IIT Study Analysis', function () {
    it('widget check', function () {
        cy.visit('index#StudyScreen_IIT')
        cy.wait(20000)

        // widget check
        cy.tablecheck('#datatable_12951')
        cy.tablecheck('#datatable_12950')
        cy.testcharts('#c12952 > svg')
        cy.tablecheck('#datatable_12952')
        cy.tablecheck('#datatable_12962')
        cy.tablecheck('#datatable_12956')
        cy.tablecheck('#datatable_12949')

    })
    it('value check', function () {

        cy.get('#summary12951 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(25000, 35000)
        })

        cy.get('#summary22951 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(13000, 20000)

        })

        cy.get('#summary32951 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.lessThan(5000)

        })

        cy.get('#summary42951 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(4, 6)

        })
        cy.get('#summary52951 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.within(2, 4)

        })
        cy.get('#summary62951 > div > div > div.circle-tile-number.text-faded > span').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            var count = Number(val)
            expect(count).to.be.lessThan(5000)

        })


    })

    it('filter check', function () {


    })

})


describe('Material dashboard', function () {
    it('value check', function () {

        //cy.get('[href=prop_material_dashboard]').click()
        cy.visit('index#prop_material_dashboard')
        cy.wait(40000)
        let active, discountinued, total


        cy.get('#summary12973 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            active = Number(val)

        })

        cy.get('#summary22973 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            discountinued = Number(val)

        })

        cy.get('#summary32973 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            total = Number(val)
            expect(total).to.be.equal((active + discountinued))

        })


    })


    it('check chart', function () {
        let activeChartValue, discountinuedChartValue, active, discountinued

        cy.get('#c12973 > svg > g:nth-child(1) > g.sub._0 > g > g > rect:nth-child(1)').trigger('mouseover')

        cy.get('.d3-tip-table > tbody > :nth-child(2) > :nth-child(2)').invoke('text').then(someText => {

            var val = someText.replace(/,/, "")
            activeChartValue = Number(val)
            cy.log(activeChartValue)

        })

        cy.get('.d3-tip-table > tbody > :nth-child(3) > :nth-child(2)').invoke('text').then(someText => {

            var val = someText.replace(/,/, "")
            discountinuedChartValue = Number(val)
            cy.log(discountinued)

        })
        cy.get('#summary12973 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            active = Number(val)

        })

        cy.get('#summary22973 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            discountinued = Number(val)
            expect(activeChartValue).to.be.equal(active)
            expect(discountinuedChartValue).to.be.equal(discountinued)

        })


    })



})

describe('Planned dashboard', function () {
    it('check all charts', function () {
        cy.visit('index#Planned_order_dashboard')
        cy.wait(20000)
        cy.testcharts('#c12994container > svg')
        cy.testcharts('#c13007container > svg')
        cy.testcharts('#c12995container > svg')
        cy.testcharts('#c13021 > svg')
        cy.testcharts('#c12988container > svg')


    })
    it('Check created date in Refine search', function () {
        cy.get('.refineSearch').click()
        cy.wait(2000)
        var BeforeDate = Cypress.moment().subtract(18, 'month').format('MM/DD/YYYY')
        cy.get('#f8 > label > div > div:nth-child(1) > label>input').should('have.value', BeforeDate)


    })
    /*  it('check planned orders by material type',function(){
 
 
     }) */


})

//code by swopna



describe('FIRM_ORDER Dashboard Page Load', function () {
    it('Testing widgets', function () {
        cy.visit('index#firm_order_dashboard')
        cy.wait(15000)

    })

    it('window SCROLL', function () {
        cy.window().scrollTo('bottom')
        cy.window().scrollTo('top')
    })


    it('Filter / Clear Refine', function () {
        cy.window().scrollTo('top')
        cy.wait(2000)
        cy.get('#actionDiv > div > a.refineSearch').click()
        cy.wait(10000)
        cy.get('#f1 > label > div > div > button').click()
        cy.get('#f1 > label > div > div > ul > div > input').type('II-')
        cy.get('#f1 > label > div > div > ul > div > input').clear()
        cy.get('#f6 > label > div > div:nth-child(1) > label>input').clear()
        //cy.get('#f1 > label > div > div > ul > div > input').type('{enter}')
        cy.get('#applyFilter').click({ force: true })
        cy.wait(20000)

    })

    it('Firm Orders by Order', function () {
        cy.get('#c13024container > svg').and(chart => {
            expect(chart.height()).to.be.greaterThan(100)
        })
    })

    it('Firm Orders by Material Type', function () {
        cy.get('#c13030container > svg').and(chart => {
            expect(chart.height()).to.be.greaterThan(50)
        })
    })

    it('Firm Orders by Order Type', function () {

        cy.get('#c13028 > svg').and(chart => {
            expect(chart.width()).to.be.greaterThan(100)
            expect(chart.height()).to.be.greaterThan(100)
        })

    })

    it('OTIF by Firm Order Type', function () {
        cy.get('#c13020container').and(chart => {
            // CHART HEIGHT IN PIXELS - we can assert anything about the chart really
            expect(chart.height()).to.be.greaterThan(100)
        })
    })

    it('Firm Orders by Vendor', function () {
        cy.get('#c13003container > svg').and(chart => {
            expect(chart.width()).to.be.greaterThan(100)
        })
    })

    it('Firm Order Cycle Time by Compound', function () {

        cy.testcharts('#c13022 > svg')
    })

    it('Firm Order Cycle Time by Order Type', function () {
        cy.testcharts('#c23022 > svg')

    })


    it('Excel/Csv_Download', function () {

        cy.get('#actionPlaceHolder > #downloadExcel').should('be.visible').click({ force: true })
        cy.wait(5000)
        cy.get('#actionPlaceHolder > #downloadCSV').should('be.visible').click({ force: true })
        cy.wait(5000)
    })

    it('Firm Orders by Order Status MATCH WITH SUMMARY', function () {
        cy.wait(5000)
        cy.get('#c13024container > svg > g > g.chart-body > g.stack._0 > text:nth-child(5)').invoke('text').then(someText => {
            var open_val = someText.replace(/,/, "")
            var open_count = Number(open_val)

            //reading summary data from json file
            cy.readFile('example.json').then((data) => {
                var open_firm_count = Number(data.open_firm_orders)
                expect(open_count).to.equal(open_firm_count)

            })
        })
    })


    it('Highest (Recieved) Order Status Count Check', function () {

        cy.get('#c13024container > svg > g > g.chart-body > g.stack._0 > text:nth-child(4)').invoke('text').then(someText => {
            var cancelled_val = someText.replace(/,/, "")
            var cancelled_count = Number(cancelled_val)

            cy.get('#c13024container > svg > g > g.chart-body > g.stack._0 > text:nth-child(5)').invoke('text').then(someText => {
                var open_val = someText.replace(/,/, "")
                var open_count = Number(open_val)

                cy.get('#c13024container > svg > g > g.chart-body > g.stack._0 > text:nth-child(6)').invoke('text').then(someText => {
                    var received_val = someText.replace(/,/, "")
                    var received_count = Number(received_val)

                    expect(received_count).to.be.greaterThan(open_count)
                    expect(received_count).to.be.greaterThan(cancelled_count)

                })
            })
        })
    })

})



describe('OBSCELENCE Dashboard Page Load', function () {

    it('Getting Inside', function () {

        cy.visit('index#KPI_Obsolescence')
        cy.wait(15000)
    })

    it('window SCROLL', function () {
        cy.window().scrollTo('bottom')
        cy.window().scrollTo('top')
    })

    it('Compound Depot+Site = Total %', function () {
        cy.get('#summary32921 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/%/, "")
            var cd_count = Number(val)
            expect(cd_count).to.be.greaterThan(0.00)


            cy.get('#summary42921 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
                var val = someText.replace(/%/, "")
                var cs_count = Number(val)
                expect(cs_count).to.be.greaterThan(0.00)


                cy.get('#summary52921 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
                    var val = someText.replace(/%/, "")
                    var ct_count = Number(val)
                    expect(ct_count).to.be.greaterThan(0.00)
                    expect((cd_count + cs_count).toFixed(2)).to.be.equal(ct_count.toFixed(2))

                })
            })
        })
    })

    it('Study Depot+Site = Total %', function () {
        cy.get('#summary12923 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var val = someText.replace(/%/, "")
            var sd_count = Number(val)
            expect(sd_count).to.be.greaterThan(0.00)


            cy.get('#summary22923 > div > div.row.no-margin > div.col-xs-12.font-bold > h2').invoke('text').then(someText => {
                var val = someText.replace(/%/, "")
                var ss_count = Number(val)
                expect(ss_count).to.be.greaterThan(0.00)


                cy.get('#summary32923 > div > div.row.no-margin > div.col-xs-12.font-bold > h2').invoke('text').then(someText => {
                    var val = someText.replace(/%/, "")
                    var st_count = Number(val)
                    expect(st_count).to.be.greaterThan(0.00)
                    expect((sd_count + ss_count).toFixed(2)).to.be.equal(st_count.toFixed(2))

                })
            })
        })

    })

    it('Target > Average - Compound', function () {

        cy.get('#c12936 > svg > g:nth-child(1) > g.sub._0 > g > g.stack._1 > rect:nth-child(1)').trigger('mouseover').wait(2000)
        cy.get('.d3-tip-table > tbody > :nth-child(4) > :nth-child(2)').invoke('text').then(someText => {
            cy.get('.d3-tip-table > tbody > :nth-child(5) > :nth-child(2)').invoke('text').then(someInnerText => {
                var target = Number(someText)
                var average = Number(someInnerText)
                cy.wait(2000)
                expect(target).to.be.greaterThan(average)
            })
        })
    })

    it('Target > Average - Studies', function () {
        cy.get('#c12932 > svg > g:nth-child(1) > g.sub._0 > g > g.stack._1 > rect:nth-child(1)').trigger('mouseover').wait(2000)
        cy.get('[style="position: absolute; top: 374px; opacity: 1; pointer-events: all; box-sizing: border-box; left: 525.425px;"] > :nth-child(1) > .d3-tip-table > tbody > :nth-child(4) > :nth-child(2)').invoke('text').then(someText => {
            cy.get('[style="position: absolute; top: 374px; opacity: 1; pointer-events: all; box-sizing: border-box; left: 525.425px;"] > :nth-child(1) > .d3-tip-table > tbody > :nth-child(5) > :nth-child(2)').invoke('text').then(someInnerText => {
                var target = Number(someText)
                var average = Number(someInnerText)
                cy.wait(2000)
                expect(target).to.be.greaterThan(average)
            })
        })

    })


}) //end of obsololence

describe('Successful Dispensations', function () {

    it('Getting Inside', function () {

        cy.visit('index#KPI_Dosage')
        cy.wait(15000)

    })

    it('window SCROLL', function () {
        cy.window().scrollTo('bottom')
        cy.window().scrollTo('top')
    })

    it('Total dispensations should be in the range of 30k-40k - 15-20K Now ', function () {
        cy.get('#summary12944 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var total_val = someText.replace(/,/, "")
            var total_count = Number(total_val)

            expect(total_count).to.be.greaterThan(15000)
            expect(total_count).to.be.lessThan(16000)
        })
    })

    it('Dispensations supported = 100%', function () {
        cy.get('#summary12945 > div > div.row.no-margin > div.col-xs-9.font-bold > h2').invoke('text').then(someText => {
            var total_val = someText.replace(/%/, "")
            var total_count = Number(total_val)

            expect(total_count).to.be.equal(100.00)
        })
    })

})



describe('forecast varience', function () {
    it('5 quaters check', function () {
        cy.visit('index#forecast_variance_dashboard')
        cy.wait(30000)
        // 5 quaters check
        cy.get('#c13034 > svg > g:nth-child(1) > g.sub._0 > g > g').children().should('have.length', 5)


    })

    it('varience check', function () {
        let summaryCurrentValue, summaryPreviousValue, varianceSummary, varienceAcutal
        cy.get('#c13034 > svg > g:nth-child(1) > g.sub._0 > g > g > rect:nth-child(1)').trigger('mouseover')
        cy.get('.d3-tip-table > tbody > :nth-child(2) > :nth-child(2)').invoke('text').then(someText => {
            var val = someText.replace(/,/, "")
            summaryPreviousValue = Number(val)

            cy.get('.d3-tip-table > tbody > :nth-child(3) > :nth-child(2)').invoke('text').then(someInnerText => {
                var val = someInnerText.replace(/,/, "")
                var summaryCurrentValueRoundoff = Number(val)
                summaryCurrentValue = summaryCurrentValueRoundoff.toFixed(2)

                cy.get('#c13037 > svg > g:nth-child(1) > g.sub._0 > g > g.dc-tooltip-list > g > circle:nth-child(3)').trigger('mouseover')
                cy.get('[style="position: absolute; top: 286px; opacity: 1; pointer-events: all; box-sizing: border-box; left: 592.85px;"] > :nth-child(1) > .d3-tip-table > tbody > :nth-child(2) > :nth-child(2)').invoke('text').then(someInnerNestedText => {
                    cy.log(someInnerNestedText)
                    varienceAcutal = someInnerNestedText
                    cy.wait(4000)
                    var varianceDiff = summaryCurrentValue - summaryPreviousValue
                    varianceSummary = (varianceDiff * 100) / summaryPreviousValue
                    var varianceSummaryRoundoff = varianceSummary.toFixed(2)
                    expect(varienceAcutal).to.be.equal(varianceSummaryRoundoff)
                    cy.wait(4000)

                })
            })

        })

    })

})

describe('Forecast accuracy', function () {
    it('check charts', function () {
        cy.visit('index#image_accuracy_dashboard')
        cy.wait(40000)

        //chartcheck
        cy.testcharts('#c12998 > svg')
        cy.testcharts('#c13058container > svg')
        cy.testcharts('#c13045 > svg')
        cy.testcharts('#c13055container > svg')

        //table check
        cy.tablecheck('#datatable_13056')

    })
})

describe('Drug wastage', function () {
    it('check charts', function () {
        cy.visit('index#usage_wastage_dashboard')
        cy.wait(30000)

        cy.testcharts('#c13040 > svg')
        cy.testcharts('#c13041 > svg')
        cy.testcharts('#c13048 > svg')
        cy.testcharts('#c13051 > svg')

        //chartcheck by  compound for active lots
        cy.get('#c13040 > svg > g:nth-child(1) > g.sub._0 > g > g.stack._3 > rect:nth-child(1)').trigger('mouseover').wait(8000)
        cy.get('.d3-tip-table > tbody > :nth-child(2) > :nth-child(2)').invoke('text').then(someText => {
            cy.get('.d3-tip-table > tbody > :nth-child(3) > :nth-child(2)').invoke('text').then(someTextInner => {
                cy.get('.d3-tip-table > tbody > :nth-child(4) > :nth-child(2)').invoke('text').then(someTextNestedInner => {
                    cy.wait(4000)
                    var used = Number(someTextInner)
                    var dns = Number(someText)
                    var onHand = Number(someTextNestedInner)
                    cy.wait(4000)
                    expect(dns + used + onHand).to.be.equal(100)
                })
            })

        })
    })
})





